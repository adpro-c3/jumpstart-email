package id.c3.adpro.email.core.message;

public interface Message {

    String make(String[] args);
}
