package id.c3.adpro.email.core.message;

public class JobNotificationSubject implements Message {

    @Override
    public String make(String[] args) {
        return "New Job at Jumpstart Is Now Available For You To Apply";
    }
}
