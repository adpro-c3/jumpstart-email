package id.c3.adpro.email.core.factory;

import id.c3.adpro.email.core.message.JobApplicationBody;
import id.c3.adpro.email.core.message.JobApplicationSubject;
import id.c3.adpro.email.core.message.Message;

public class JobApplicationMailFactory implements MailFactory {

    @Override
    public Message getSubject() {
        return new JobApplicationSubject();
    }

    @Override
    public Message getBody() {
        return new JobApplicationBody();
    }

}
