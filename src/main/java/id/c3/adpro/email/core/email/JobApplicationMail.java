package id.c3.adpro.email.core.email;

import id.c3.adpro.email.core.factory.JobApplicationMailFactory;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class JobApplicationMail extends Email {

    /**
     * Email template to notify job application's user
     *
     * @param args [recipient address, recipient name, job name, application status]
     */
    public JobApplicationMail(MimeMessage mimeMessage, String[] args) throws
        MessagingException {
        super(mimeMessage);
        this.mailFactory = new JobApplicationMailFactory();
        make(args, true);
    }
}
