package id.c3.adpro.email.core.email;

import id.c3.adpro.email.core.factory.JobNotificationMailFactory;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class JobNotificationMail extends Email {

    /**
     * Email template to notify user. whenever new job with matching skill is verified.
     *
     * @param args [recipient address, recipient name, job name, application status]
     */
    public JobNotificationMail(MimeMessage mimeMessage, String[] args) throws MessagingException {
        super(mimeMessage);
        this.mailFactory = new JobNotificationMailFactory();
        make(args, true);
    }
}
