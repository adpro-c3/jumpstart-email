package id.c3.adpro.email.core.message;

public class JobApplicationSubject implements Message {

    @Override
    public String make(String[] args) {
        return "Notice About Your Application at Jumpstart!";
    }
}
