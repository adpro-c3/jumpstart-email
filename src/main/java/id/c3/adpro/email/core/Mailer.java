package id.c3.adpro.email.core;

import id.c3.adpro.email.core.email.Email;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSender;

public class Mailer implements Runnable {

    public static boolean running;
    private static LinkedBlockingQueue<Email> pendingMails;
    private static JavaMailSender sender;
    private Logger LOG = LoggerFactory.getLogger(Mailer.class);

    public Mailer() {
    }

    public static void registerSharedQueue(LinkedBlockingQueue<Email> queue) {
        pendingMails = queue;
    }

    public static void registerJavaMailer(JavaMailSender javaSender) {
        sender = javaSender;
    }

    @Override
    public void run() {
        running = true;
        try {
            while (true) {
                Email mail = pendingMails.take();
                if (mail != null) {
                    try {
                        sender.send(mail.getMimeMessage());
                    } catch (Exception e) {
                        LOG.error("Error while sending an email", e);
                    }
                }
            }
        } catch (InterruptedException e) {
            LOG.warn("Mailer thread interrupted", e);
            Thread.currentThread().interrupt();
        }
    }
}
