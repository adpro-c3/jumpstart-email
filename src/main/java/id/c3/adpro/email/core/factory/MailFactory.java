package id.c3.adpro.email.core.factory;

import id.c3.adpro.email.core.message.Message;

public interface MailFactory {

    Message getSubject();

    Message getBody();
}
