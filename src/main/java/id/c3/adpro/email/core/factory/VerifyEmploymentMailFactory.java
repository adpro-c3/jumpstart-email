package id.c3.adpro.email.core.factory;

import id.c3.adpro.email.core.message.Message;
import id.c3.adpro.email.core.message.VerifyEmploymentBody;
import id.c3.adpro.email.core.message.VerifyEmploymentSubject;

public class VerifyEmploymentMailFactory implements MailFactory {

    @Override
    public Message getSubject() {
        return new VerifyEmploymentSubject();
    }

    @Override
    public Message getBody() {
        return new VerifyEmploymentBody();
    }

}
