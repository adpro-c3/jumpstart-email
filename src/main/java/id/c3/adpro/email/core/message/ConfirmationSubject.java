package id.c3.adpro.email.core.message;

public class ConfirmationSubject implements Message {

    @Override
    public String make(String[] args) {
        return "Verify your Jumpstart account!";
    }
}
