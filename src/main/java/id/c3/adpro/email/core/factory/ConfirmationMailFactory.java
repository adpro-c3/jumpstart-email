package id.c3.adpro.email.core.factory;

import id.c3.adpro.email.core.message.ConfirmationBody;
import id.c3.adpro.email.core.message.ConfirmationSubject;
import id.c3.adpro.email.core.message.Message;

public class ConfirmationMailFactory implements MailFactory {

    @Override
    public Message getSubject() {
        return new ConfirmationSubject();
    }

    @Override
    public Message getBody() {
        return new ConfirmationBody();
    }
}
