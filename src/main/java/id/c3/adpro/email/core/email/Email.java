package id.c3.adpro.email.core.email;

import id.c3.adpro.email.core.factory.MailFactory;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.javamail.MimeMessageHelper;

public abstract class Email extends MimeMessageHelper {

    protected MailFactory mailFactory;

    public Email(MimeMessage mimeMessage) {
        super(mimeMessage);
    }

    public void make(String[] args, boolean isHtml) throws MessagingException {
        this.setTo(args[0]);
        this.setSubject(mailFactory.getSubject().make(args));
        this.setText(mailFactory.getBody().make(args), isHtml);
    }
}
