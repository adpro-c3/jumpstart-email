package id.c3.adpro.email.core.factory;

import id.c3.adpro.email.core.message.JobNotificationBody;
import id.c3.adpro.email.core.message.JobNotificationSubject;
import id.c3.adpro.email.core.message.Message;

public class JobNotificationMailFactory implements MailFactory {

    @Override
    public Message getSubject() {
        return new JobNotificationSubject();
    }

    @Override
    public Message getBody() {
        return new JobNotificationBody();
    }

}
