package id.c3.adpro.email.core.message;

public class VerifyEmploymentSubject implements Message {

    @Override
    public String make(String[] args) {
        return "Your Job Recruitment Verification Process has ended!";
    }
}
