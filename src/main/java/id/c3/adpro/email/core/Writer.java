package id.c3.adpro.email.core;

import id.c3.adpro.email.core.email.ConfirmationMail;
import id.c3.adpro.email.core.email.Email;
import id.c3.adpro.email.core.email.JobApplicationMail;
import id.c3.adpro.email.core.email.JobNotificationMail;
import id.c3.adpro.email.core.email.VerifyEmploymentMail;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import org.springframework.mail.javamail.JavaMailSender;

public class Writer {

    private static BlockingQueue<Email> mailQueue;
    private static String thisdomain;

    /**
     * Email factory, pass result into a queue.
     *
     * @param type type of email
     * @param args array of string, args[0] is always the recipient address.
     */
    public static void write(String type, String domain, JavaMailSender sender, String[] args)
        throws
        MessagingException {
        thisdomain = domain;
        MimeMessage newMessage = sender.createMimeMessage();
        switch (type) {
            case "confirmation":
                mailQueue.add(new ConfirmationMail(newMessage, args));
                break;
            case "application":
                mailQueue.add(new JobApplicationMail(newMessage, args));
                break;
            case "notification":
                mailQueue.add(new JobNotificationMail(newMessage, args));
                break;
            case "verify":
                mailQueue.add(new VerifyEmploymentMail(newMessage, args));
                break;
            default:
        }
    }

    public static String getUri() {
        return "http://" + thisdomain;
    }

    public static void registerSharedQueue(LinkedBlockingQueue<Email> queue) {
        mailQueue = queue;
    }
}
