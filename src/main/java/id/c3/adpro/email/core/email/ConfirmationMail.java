package id.c3.adpro.email.core.email;

import id.c3.adpro.email.core.factory.ConfirmationMailFactory;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class ConfirmationMail extends Email {
    /**
     * Email template to confirm an account
     *
     * @param args [recipient address, recipient name, token]
     */
    public ConfirmationMail(MimeMessage mimeMessage, String[] args) throws MessagingException {
        super(mimeMessage);
        this.mailFactory = new ConfirmationMailFactory();
        make(args, true);
    }
}
