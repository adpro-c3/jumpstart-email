package id.c3.adpro.email.core.email;

import id.c3.adpro.email.core.factory.VerifyEmploymentMailFactory;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class VerifyEmploymentMail extends Email {

    /**
     * Email template to notify job employer about their job employment status
     *
     * @param args [recipient address, recipient name, job name, employment status]
     */
    public VerifyEmploymentMail(MimeMessage mimeMessage, String[] args) throws MessagingException {
        super(mimeMessage);
        this.mailFactory = new VerifyEmploymentMailFactory();
        make(args, true);
    }
}
