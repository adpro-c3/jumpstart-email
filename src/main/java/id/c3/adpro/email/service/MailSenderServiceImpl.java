package id.c3.adpro.email.service;

import id.c3.adpro.email.core.Mailer;
import id.c3.adpro.email.core.Writer;
import javax.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class MailSenderServiceImpl implements MailSenderService {

    @Autowired
    private JavaMailSender javaMailSender;

    private Logger LOG = LoggerFactory.getLogger(MailSenderService.class);

    @Value("${service.domain}")
    private String domain;

    private Thread mailer = new Thread(new Mailer(), "mailer");

    @Override
    public void setJavaMailSender(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMail(String type, String[] args) {
        try {
            Writer.write(type, domain, javaMailSender, args);
        } catch (MessagingException e) {
            LOG.error("Failed to send message", e);
        }
        if (!Mailer.running) {
            Mailer.registerJavaMailer(javaMailSender);
            Mailer.running = true;
            mailer.start();
        }
    }

    @Override
    public void sendConfirmationMail(String email, String nama, String confirmationToken) {

        sendMail("confirmation", new String[] {
            email,
            nama,
            confirmationToken});
    }


    @Override
    public void sendJobApplicationMail(String email, String namaPengguna, String namaJob,
        String status) {
        sendMail("application", new String[] {
            email,
            namaPengguna,
            namaJob,
            status});
    }

    @Override
    public void sendJobNotificationMail(String email, String namaPengguna, String namaJob,
        String skills) {
        sendMail("notification", new String[] {
            email,
            namaPengguna,
            namaJob,
            skills});
    }

    @Override
    public void sendVerifyJobEmploymentMail(String email, String namaEmployer, String namaJob,
        String status) {
        sendMail("verify", new String[] {
            email,
            namaEmployer,
            namaJob,
            String.valueOf(status)});
    }

}
