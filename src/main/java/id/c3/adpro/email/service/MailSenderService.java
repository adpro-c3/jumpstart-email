package id.c3.adpro.email.service;

import org.springframework.mail.javamail.JavaMailSender;

public interface MailSenderService {

    void setJavaMailSender(JavaMailSender javaMailSender);

    void sendConfirmationMail(String email, String nama, String args);

    void sendJobApplicationMail(String email, String namaApplicant, String namaJob, String status);

    void sendJobNotificationMail(String email, String namaPengguna, String namaJob, String skills);

    void sendVerifyJobEmploymentMail(String email, String namaEmployer, String namaJob,
        String status);
}
