package id.c3.adpro.email.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.c3.adpro.email.model.MailRequest;
import id.c3.adpro.email.service.MailSenderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificationController {

    @Autowired
    MailSenderServiceImpl mailSenderService;

    @Autowired
    private ObjectMapper objectMapper;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<?> homePage() throws JsonProcessingException {
        String[] args = new String[] {"this", "is", "an", "example"};
        MailRequest example = new MailRequest("dummy@mcdombdomb.com", "Dummy", args);
        return ResponseEntity.ok(objectMapper.writeValueAsString(example));
    }

    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public ResponseEntity<?> ping() {
        return ResponseEntity.ok("pong");
    }

    @RequestMapping(value = "/confirmationMail", method = RequestMethod.POST)
    public ResponseEntity<?> sendRegisterEmail(@RequestBody MailRequest mailRequest) {
        try{
            mailSenderService.sendConfirmationMail(
                mailRequest.getEmail(),
                mailRequest.getName(),
                mailRequest.getArgs()[0]
            );
            return ResponseEntity.ok("Registrasi Berhasil, silakan cek email Anda untuk konfirmasi");
        } catch (NullPointerException npe){
            return ResponseEntity.badRequest().body("Data yang dikirim tidak valid, registrasi gagal.");
        }
    }

    @RequestMapping(value = "/notifyMail", method = RequestMethod.POST)
    public ResponseEntity<?> sendNotifyMail(@RequestBody MailRequest mailRequest) {
        try {
            mailSenderService.sendJobNotificationMail(
                mailRequest.getEmail(),
                mailRequest.getName(),
                mailRequest.getArgs()[0],
                mailRequest.getArgs()[1]
            );
            return ResponseEntity.ok("Notifikasi Verified New Job berhasil dikirim");
        } catch (NullPointerException npe){
            return ResponseEntity.badRequest().body("Data yang dikirim tidak valid, registrasi gagal.");
        }
    }

    @RequestMapping(value = "/recruitmentMail", method = RequestMethod.POST)
    public ResponseEntity<?> sendEmploymentMail(@RequestBody MailRequest mailRequest) {
        try {
            mailSenderService.sendVerifyJobEmploymentMail(
                mailRequest.getEmail(),
                mailRequest.getName(),
                mailRequest.getArgs()[0],
                mailRequest.getArgs()[1]
            );
            return ResponseEntity.ok("Notifikasi Job Recruitment Verification berhasil dikirim");
        } catch (NullPointerException npe){
            return ResponseEntity.badRequest().body("Data yang dikirim tidak valid, registrasi gagal.");
        }
    }

    @RequestMapping(value = "/applicationMail", method = RequestMethod.POST)
    public ResponseEntity<?> sendJobApplicationMail(@RequestBody MailRequest mailRequest) {
        try {
            mailSenderService.sendJobApplicationMail(
                mailRequest.getEmail(),
                mailRequest.getName(),
                mailRequest.getArgs()[0],
                mailRequest.getArgs()[1]
            );
            return ResponseEntity.ok("Notifikasi Job Application berhasil dikirim");
        } catch (NullPointerException npe){
            return ResponseEntity.badRequest().body("Data yang dikirim tidak valid, registrasi gagal.");
        }
    }
}

