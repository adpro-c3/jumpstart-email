package id.c3.adpro.email.model;

import java.io.Serializable;

public class MailRequest implements Serializable {

    private String email;
    private String name;
    private String[] args;

    public MailRequest() {
    }

    public MailRequest(String email, String name, String[] args) {
        this.email = email;
        this.name = name;
        this.args = args;
    }

    public String getEmail() {
        return this.email;
    }

    public String getName() {
        return this.name;
    }

    public String[] getArgs() {
        return this.args;
    }
}