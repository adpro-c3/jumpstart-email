package id.c3.adpro.email;

import id.c3.adpro.email.core.Mailer;
import id.c3.adpro.email.core.Writer;
import id.c3.adpro.email.core.email.Email;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class EmailApplication {

    public static Logger LOG = LoggerFactory.getLogger(EmailApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(EmailApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void postStartup() {
        LOG.info("Starting post startup subroutine");
        long start = System.currentTimeMillis();
        LinkedBlockingQueue<Email> emails = new LinkedBlockingQueue<>();
        Writer.registerSharedQueue(emails);
        Mailer.registerSharedQueue(emails);
        long end = System.currentTimeMillis();
        LOG.info(String.format("Finished starting up in %.3f seconds", (end - start) / 1000f));
    }

}
