package id.c3.adpro.email.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import id.c3.adpro.email.core.Mailer;
import id.c3.adpro.email.core.Writer;
import id.c3.adpro.email.core.email.Email;
import java.util.concurrent.LinkedBlockingQueue;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.print.attribute.standard.MediaSize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

@ExtendWith(MockitoExtension.class)
public class MailSenderServiceImplTest {

    private final String EMAIL = "dummy@mail.com";
    private final String NAME = "dummyname";
    private JavaMailSender sender;

    private MailSenderServiceImpl mailSenderService;

    private MailSenderServiceImpl mailSenderServiceSpy;

    /*@Mock
    private Writer writer;

    @Mock
    private Mailer mailer;*/

    @BeforeEach
    public void setUp() throws Exception {
        mailSenderService = new MailSenderServiceImpl();
        mailSenderServiceSpy = spy(mailSenderService);
        sender = mock(JavaMailSender.class);
        mailSenderServiceSpy.setJavaMailSender(sender);
        when(sender.createMimeMessage()).thenReturn(new MimeMessage((Session) null));
        LinkedBlockingQueue<Email> emails = new LinkedBlockingQueue<>();
        Writer.registerSharedQueue(emails);
        Mailer.registerSharedQueue(emails);
    }

    @Test
    public void testSendConfirmationMailShouldSucceed() {
        String[] confirmationArgs = new String[]{EMAIL, NAME, "dummytoken"};
        mailSenderServiceSpy.sendConfirmationMail(EMAIL, NAME, "dummytoken");

        verify(mailSenderServiceSpy, times(1)).sendMail("confirmation", confirmationArgs);
    }

    @Test
    public void testSendJobApplicationShouldSucceed() {
        String[] applicationArgs = new String[]{EMAIL, NAME, "JobName", "diterima"};
        mailSenderServiceSpy.sendJobApplicationMail(EMAIL, NAME, "JobName", "diterima");
        verify(mailSenderServiceSpy, times(1)).sendMail("application", applicationArgs);
    }

    @Test
    public void testSendJobNotificationShouldSucceed() {
        String[] notificationArgs = new String[]{EMAIL, NAME, "JobName", "Skills"};
        mailSenderServiceSpy.sendJobNotificationMail(EMAIL, NAME, "JobName", "Skills");
        verify(mailSenderServiceSpy, times(1)).sendMail("notification", notificationArgs);
    }

    @Test
    public void testSendVerifyJobEmploymentShouldSucceed() {
        String[] verifyArgs = new String[]{EMAIL, NAME, "JobName", "true"};
        mailSenderServiceSpy.sendVerifyJobEmploymentMail(EMAIL, NAME, "JobName", "true");
        verify(mailSenderServiceSpy, times(1)).sendMail("verify", verifyArgs);
    }

    /*@Test void testFakeDomainSendMailShouldThrowException(){
        mailSenderService.sendConfirmationMail("fakemail", NAME, "dummytoken");
    }*/

}
