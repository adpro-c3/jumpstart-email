package id.c3.adpro.email.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.c3.adpro.email.service.MailSenderServiceImpl;
import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class NotificationControllerTest {

    private final String RECIPIENT = "recipient@mail.com";
    private final String SUBJECT = "dummysubject";

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MailSenderServiceImpl mailSenderService;

    private String registerDataJson(Boolean valid, String[] mailArgs) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("email", RECIPIENT);
        mapData.put("nama", SUBJECT);

        if(valid) mapData.put("args", mailArgs);

        return objectMapper.writeValueAsString(mapData);
    }


    @Test
    public void testPingShouldReturnPong() throws Exception{
        mvc.perform(get("/ping")
        .contentType(MediaType.ALL))
        .andExpect(status().isOk())
        .andExpect(content().string("pong"));
    }

    @Test
    public void testHomepageShouldReturnMcDumbDumbGuideline() throws Exception{
        mvc.perform(get("/")
        .contentType(MediaType.ALL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.email").value("dummy@mcdombdomb.com"))
        .andExpect(jsonPath("$.name").value("Dummy"))
        .andExpect(jsonPath("$.args").isArray());
    }

    @Test
    public void testValidConfirmationMailRequestShouldWorks() throws Exception{
        String content = registerDataJson(true, new String[]{"validtoken"});
        mvc.perform(post("/confirmationMail")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect(status().isOk())
            .andExpect(content().string("Registrasi Berhasil, silakan cek email Anda untuk konfirmasi"));
    }

    @Test
    public void testInvalidConfirmationMailRequestShouldNotWorks() throws Exception{
        String content = registerDataJson(false, null);
        mvc.perform(post("/confirmationMail")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Data yang dikirim tidak valid, registrasi gagal."));
    }

    @Test
    public void testValidNotifyMailRequestShouldWorks() throws Exception{
        String content = registerDataJson(true,new String[]{"jobname", "skill"});
        mvc.perform(post("/notifyMail")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect(status().isOk())
            .andExpect(content().string("Notifikasi Verified New Job berhasil dikirim"));
    }

    @Test
    public void testInvalidNotifyMailRequestShouldNotWorks() throws Exception{
        String content = registerDataJson(false, null);
        mvc.perform(post("/notifyMail")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Data yang dikirim tidak valid, registrasi gagal."));
    }

    @Test
    public void testValidEmploymentMailRequestShouldWorks() throws Exception{
        String content = registerDataJson(true, new String[]{"namajob", "true"});
        mvc.perform(post("/recruitmentMail")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect(status().isOk())
            .andExpect(content().string("Notifikasi Job Recruitment Verification berhasil dikirim"));
    }

    @Test
    public void testInvalidEmploymentMailRequestShouldNotWorks() throws Exception{
        String content = registerDataJson(false, null);
        mvc.perform(post("/recruitmentMail")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Data yang dikirim tidak valid, registrasi gagal."));
    }

    @Test
    public void testValidApplicationMailRequestShouldWorks() throws Exception{
        String content = registerDataJson(true, new String[]{"namajob", "status"});
        mvc.perform(post("/applicationMail")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect(status().isOk())
            .andExpect(content().string("Notifikasi Job Application berhasil dikirim"));
    }

    @Test
    public void testInvalidApplicationMailRequestShouldNotWorks() throws Exception{
        String content = registerDataJson(false, null);
        mvc.perform(post("/applicationMail")
            .contentType(MediaType.APPLICATION_JSON)
            .content(content))
            .andExpect(status().isBadRequest())
            .andExpect(content().string("Data yang dikirim tidak valid, registrasi gagal."));
    }



}
