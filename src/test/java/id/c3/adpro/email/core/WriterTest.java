package id.c3.adpro.email.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import id.c3.adpro.email.core.email.Email;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

@ExtendWith(MockitoExtension.class)
public class WriterTest {

    private LinkedBlockingQueue<Email> emails;
    private JavaMailSender sender;
    private MimeMessage mimeMessage;
    private BlockingQueue<Email> mailQueue;

    @BeforeEach
    public void setUp() {
        emails = new LinkedBlockingQueue<>();
        mimeMessage = new MimeMessage((Session) null);
        sender = mock(JavaMailSender.class);
        lenient().when(sender.createMimeMessage()).thenReturn(mimeMessage);
    }

    @Test
    public void testCreateWriterObject() {
        Writer writer = new Writer();
        assertEquals("id.c3.adpro.email.core.Writer", writer.getClass().getName());
    }

    @Test
    public void testWriterWriteConfirmationMailShouldContainCorrectData() throws Exception {
        Writer.registerSharedQueue(emails);

        String[] dummyargs = new String[] {"Penerima", "nama", "token"};
        Writer.write("confirmation", "dummydomain.com", sender, dummyargs);

        verify(sender, times(1)).createMimeMessage();
        assertEquals("http://dummydomain.com", Writer.getUri());
    }


}
