package id.c3.adpro.email.core.email;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

@ExtendWith(MockitoExtension.class)
public class JobApplicationTest {
    private JavaMailSender sender;
    private MimeMessage mimeMessage;

    private JobApplicationMail jobApplicationMail;
    private final String[] MAILCONTENTACC = new String[]{"Penerima", "Nama", "Jobname", "diterima"};
    private final String[] MAILCONTENTDEC = new String[]{"Penerima", "Nama", "Jobname", "dec"};

    @BeforeEach
    public void setUp() {
        mimeMessage = new MimeMessage((Session) null);
        sender = mock(JavaMailSender.class);
        lenient().when(sender.createMimeMessage()).thenReturn(mimeMessage);
    }

    @Test
    public void testJobApplicationAccConstructorShouldWorks() throws Exception {
        jobApplicationMail = new JobApplicationMail(
            sender.createMimeMessage(),
            MAILCONTENTACC);

        assertEquals("id.c3.adpro.email.core.factory.JobApplicationMailFactory",
            jobApplicationMail.mailFactory.getClass().getName());
    }

    @Test
    public void testJobApplicationDecConstructorShouldWorks() throws Exception{
        jobApplicationMail = new JobApplicationMail(
            sender.createMimeMessage(),
            MAILCONTENTDEC);

        assertEquals("id.c3.adpro.email.core.factory.JobApplicationMailFactory",
            jobApplicationMail.mailFactory.getClass().getName());
    }
}
