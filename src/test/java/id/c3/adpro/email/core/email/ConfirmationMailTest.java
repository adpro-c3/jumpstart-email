package id.c3.adpro.email.core.email;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

@ExtendWith(MockitoExtension.class)
public class ConfirmationMailTest {

    private JavaMailSender sender;
    private MimeMessage mimeMessage;

    private ConfirmationMail confirmationMail;
    private final String[] MAILCONTENT = new String[]{"Penerima", "Nama", "Isi"};

    @BeforeEach
    public void setUp() {
        mimeMessage = new MimeMessage((Session) null);
        sender = mock(JavaMailSender.class);
        lenient().when(sender.createMimeMessage()).thenReturn(mimeMessage);
    }

    @Test
    public void testConfirmationMailConstructorShouldWorks() throws Exception {
        confirmationMail = new ConfirmationMail(
            sender.createMimeMessage(),
            MAILCONTENT);

        assertEquals("id.c3.adpro.email.core.factory.ConfirmationMailFactory",
            confirmationMail.mailFactory.getClass().getName());
    }
}
