package id.c3.adpro.email.core.email;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.javamail.JavaMailSender;

@ExtendWith(MockitoExtension.class)
public class VerifyEmploymentTest {
    private JavaMailSender sender;
    private MimeMessage mimeMessage;

    private VerifyEmploymentMail verifyEmploymentMail;
    private final String[] MAILCONTENTACC = new String[]{"Penerima", "Nama", "Jobname", "true"};
    private final String[] MAILCONTENTDEC = new String[]{"Penerima", "Nama", "Jobname", "false"};

    @BeforeEach
    public void setUp() {
        mimeMessage = new MimeMessage((Session) null);
        sender = mock(JavaMailSender.class);
        lenient().when(sender.createMimeMessage()).thenReturn(mimeMessage);
    }

    @Test
    public void testVerifyEmploymentMailAccConstructorShouldWorks() throws Exception {
        verifyEmploymentMail = new VerifyEmploymentMail(
            sender.createMimeMessage(),
            MAILCONTENTACC);

        assertEquals("id.c3.adpro.email.core.factory.VerifyEmploymentMailFactory",
            verifyEmploymentMail.mailFactory.getClass().getName());
    }

    @Test
    public void testVerifyEmploymentMailDecConstructorShouldWorks() throws Exception {
        verifyEmploymentMail = new VerifyEmploymentMail(
            sender.createMimeMessage(),
            MAILCONTENTDEC);

        assertEquals("id.c3.adpro.email.core.factory.VerifyEmploymentMailFactory",
            verifyEmploymentMail.mailFactory.getClass().getName());
    }
}
